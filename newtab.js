var bms = browser.bookmarks;
var grid = document.getElementById('grid');
var db = browser.storage.sync;
var body = document.getElementsByTagName('body')[0]

const SD_KEY = 'speed-dial-folders';

function getCard(name) {
    return `./cards/${name}.svg`;
}

var KNOWN_WEBSITES = {};
var KW_URLS = [];

function loadKnownWebsites(cb) {
    fetch('./known_websites.csv').then(
            res => { if (res.ok) return res.text(); }
    ).then(kw => {
        kw = kw.trim();
        kw.split('\n').map(row => {
            let split = row.split(';');
            KNOWN_WEBSITES[split[0]] = split[1];
        });
        KW_URLS = Object.keys(KNOWN_WEBSITES);
        cb();
    });
}

function loadFavoritesFolders(ids) {
    if (ids.length <= 0) return;
    let id = ids[0];
    bms.getChildren(
        id
    ).then(bm_res => {
        bm_res.filter(bm => bm.type === 'bookmark').forEach(bm => {
            let item = document.createElement('li');
            let host = new URL(bm.url).host;
            let matches = KW_URLS.filter(kw => host.includes(kw));
            if (matches.length > 0) {
                let card_img = getCard(KNOWN_WEBSITES[matches[0]]);
                item.style.backgroundImage = `url('${card_img}')`
            }
            else {
                let icon = document.createElement('img');
                icon.onerror = () => {icon.src = 'default_favicon.svg'}
                icon.src = `https://external-content.duckduckgo.com/ip3/${host}.ico`;
                item.appendChild(icon);
            }
            let span = document.createElement('span');
            span.appendChild(document.createTextNode(bm.title));
            item.appendChild(span);
            item.onclick = () => {location.href = bm.url;};
            grid.appendChild(item);
        });
        loadFavoritesFolders(ids.splice(1));
    });
}

function populateSettings(folders) {
    let list = document.getElementById('speed-dial-folders');
    list.innerHTML = '';
    folders.map(folder => {
        let fi = document.createElement('li');
        let span = document.createElement('span');
        span.appendChild(document.createTextNode(folder));
        fi.appendChild(span);
        let del_btn = document.createElement('button');
        del_btn.appendChild(document.createTextNode('×'));
        del_btn.onclick = () => {
            db.get({'speed-dial-folders': []}).then(res => {
                let n_folders = res[SD_KEY].filter(x => x != folder);
                db.set({'speed-dial-folders': n_folders}).then(res => {
                    populateAll();
                });
            });
        };
        fi.appendChild(del_btn);
        list.appendChild(fi);
    });
}

function populateAll() {
    grid.innerHTML = '';
    db.get({'speed-dial-folders': []}).then(async res => {
        let folders = [];
        if (res[SD_KEY].length <= 0) {
            db.set({'speed-dial-folders': ['Speed Dial']});
            folders = ['Speed Dial'];
        }
        else {
            folders = res[SD_KEY];
        }
        populateSettings(folders);
        let folder_ids = [];
        for (let folder of folders) {
            let res = await bms.search({title: folder});
            res.map(f => folder_ids.push(f.id));
        }
        loadFavoritesFolders(folder_ids);
    });
}

function toggleSettings() {
    let settings = document.getElementById('settings');
    if (settings.className.includes('open')) {
        settings.classList.remove('open');
    }
    else {
        settings.classList.add('open');
    }
}

loadKnownWebsites(populateAll);

document.getElementById('settings-toggle').onclick = toggleSettings;
document.getElementById('new-folder-btn').onclick = () => {
    let n_folder_input = document.getElementById('new-folder-input');
    let n_folder = n_folder_input.value.trim();
    n_folder_input.value = '';
    if (!n_folder) return;
    db.get({'speed-dial-folders': []}).then(res => {
        db.set({'speed-dial-folders': [...res[SD_KEY], n_folder]}).then(res => {
            populateAll();
        });
    });
};


// General function to initialize similar settings
function initSetting(set_func, input_id, on_change, reset_btn_id, default_val) {
    let input = document.getElementById(input_id);
    input.onchange = on_change;
    let reset_btn = document.getElementById(reset_btn_id);
    reset_btn.onclick = () => on_change({'target': {'value': default_val}});
    set_func();
}

// Background color setting
function setBgColor() {
    db.get({'bg-color': '#21222c'}).then(res => {
        let target = res['bg-color'];
        body.style.backgroundColor = res['bg-color'];
        document.getElementById('bgColorInput').value = target;
    });
}
function onBgColorInputChange(ev) {
    db.set({'bg-color': ev.target.value});
    setBgColor();
}
initSetting(setBgColor, 'bgColorInput', onBgColorInputChange, 'resetDefaultBgColorBtn', '#21222c');

// Text (Foreground) color setting
function setFgColor() {
    db.get({'fg-color': '#ffffff'}).then(res => {
        let target = res['fg-color'];
        body.style.color = target;
        document.getElementById('fgColorInput').value = target;
    });
}
function onFgColorInputChange(ev) {
    db.set({'fg-color': ev.target.value});
    setFgColor();
}
initSetting(setFgColor, 'fgColorInput', onFgColorInputChange, 'resetDefaultFgColorBtn', '#ffffff')

// Background image setting (using localStorage instead of addon storage)
function setBgImage() {
    let target = localStorage.getItem('bg-image');
    if (target === null) {
        body.style.backgroundImage = null;
    }
    else {
        target = target.replace(/(\r\n|\n|\r)/gm, "");
        body.style.backgroundImage = `url("${target}")`;
        body.style.backgroundSize = 'cover';
        body.style.backgroundRepeat = 'norepeat';
        body.style.backgroundPosition = 'center';
    }
}
function onBgImageChanged(ev) {
    if (!ev.target.files) {
        localStorage.setItem('bg-image', null);
        db.set({'bg-image': null});
        setBgImage();
        return;
    }
    let reader = new FileReader();
    reader.readAsDataURL(
        ev.target.files[0]
    );
    reader.onload = () => {
        localStorage.setItem('bg-image', reader.result);
        setBgImage();
    };
}
initSetting(setBgImage, 'bgImageInput', onBgImageChanged, 'resetDefaultBgImageBtn', null);

// Font family setting
function setFontFamily() {
    db.get({'font-family': 'Red Hat Display'}).then(res => {
        let target = res['font-family'];
        body.style.fontFamily = target;
        document.getElementById('fontFamilyInput').value = target;
    });
}
function onFontFamilyChanged(ev) {
    db.set({'font-family': ev.target.value});
    setFontFamily();
}
initSetting(setFontFamily, 'fontFamilyInput', onFontFamilyChanged, 'resetFontFamilyBtn', 'Red Hat Display');

// Font size setting
function setFontSize() {
    db.get({'font-size': '1.2em'}).then(res => {
        let target = res['font-size'];
        document.getElementById('grid').style.fontSize = target;
        document.getElementById('fontSizeInput').value = target;
    });
}
function onFontSizeChanged(ev) {
    db.set({'font-size': ev.target.value});
    setFontSize();
}
initSetting(setFontSize, 'fontSizeInput', onFontSizeChanged, 'resetFontSizeBtn', '1.2em');
